package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.web.sleepcoderepeat.dicomov.BuildConfig
import app.web.sleepcoderepeat.dicomov.api.ApiService
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.format
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap


@HiltAndroidTest
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P], application = HiltTestApplication::class)
class MovieViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var apiService: ApiService

    private lateinit var movieViewModel: MovieViewModel

    @Mock
    lateinit var repository: MovieTvRepository

    private val mapListQueries = HashMap<String?, String?>().apply {
        put(Constant.API_KEY, BuildConfig.API_TOKEN)
    }

    private val mockMapListQueries = HashMap<String?, String?>().apply {
        put(Constant.PRIMARY_RELEASE_DATE_KEY, Calendar.getInstance().time.format())
        put(Constant.PAGE_KEY, "1")
        put(Constant.SORT_BY_KEY, Constant.POPULARITY_DESC)
    }

    @Before
    fun init() {
        hiltRule.inject()
        MockitoAnnotations.initMocks(this)
        movieViewModel = MovieViewModel(repository)
    }

    @Test
    fun `Get Movie Data OnSuccess`() {
        val dataMovieResponse = apiService.getDiscoverMovie(mapListQueries).blockingSingle()

        `when`(repository.getDataDiscoverMovie(eq(mockMapListQueries), any()))
            .thenAnswer { it1 ->
                Observable.just(dataMovieResponse).subscribe({
                    it.body()?.apply {
                        (it1.arguments[1] as MovieTvSource.MovieListCallback).onSuccess(this)
                    }
                }, {})
            }

        movieViewModel.getListMovie()
        assertNotNull(movieViewModel.movieList.value)
        assertEquals(
            dataMovieResponse.body()?.movieListItems?.size,
            movieViewModel.movieList.value?.size
        )

    }

}