package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import android.content.Context
import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import app.web.sleepcoderepeat.dicomov.BuildConfig
import app.web.sleepcoderepeat.dicomov.api.ApiService
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.local.room.FavoriteDao
import app.web.sleepcoderepeat.dicomov.data.local.room.FavoriteDatabase
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.RequestResponse
import app.web.sleepcoderepeat.dicomov.utils.Constant
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import io.reactivex.Observable
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.io.IOException
import javax.inject.Inject

@HiltAndroidTest
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P], application = HiltTestApplication::class)
class DetailMovieViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var apiService: ApiService

    private lateinit var detailMovieViewModel: DetailMovieViewModel

    @Mock
    lateinit var repository: MovieTvRepository

    private val sampleMovieId = 724989

    private val mapListQueries = HashMap<String?, String?>().apply {
        put(Constant.API_KEY, BuildConfig.API_TOKEN)
    }

    private lateinit var localDatabase: FavoriteDatabase
    private lateinit var favoriteDao: FavoriteDao

    @Before
    fun init() {
        hiltRule.inject()
        MockitoAnnotations.initMocks(this)
        detailMovieViewModel = DetailMovieViewModel(repository)
        val context = ApplicationProvider.getApplicationContext<Context>()
        localDatabase = Room.inMemoryDatabaseBuilder(
            context, FavoriteDatabase::class.java
        ).allowMainThreadQueries().build()
        favoriteDao = localDatabase.favoriteDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        localDatabase.close()
    }

    @Test
    fun `Get Detail Movie`() {
        val dataDetailMovieResponse =
            apiService.getMovieDetails(sampleMovieId, mapListQueries).blockingSingle()

        Mockito.`when`(
            repository.getDataMovieDetails(eq(sampleMovieId), eq(HashMap()), any())
        ).thenAnswer { it1 ->
            Observable.just(dataDetailMovieResponse).subscribe({
                it.body()?.apply {
                    (it1.arguments[2] as MovieTvSource.MovieDetailCallback).onSuccess(this)
                }
            }, {})
        }

        detailMovieViewModel.getDetail(sampleMovieId)
        assertNotNull(detailMovieViewModel.movieData.value)
        assertEquals(
            dataDetailMovieResponse.body(),
            detailMovieViewModel.movieData.value
        )

    }

    @Test
    fun `Add Favorite Movie`() {
        val dataDetailMovieResponse =
            apiService.getMovieDetails(sampleMovieId, mapListQueries).blockingSingle()

        Mockito.`when`(
            repository.getDataMovieDetails(eq(sampleMovieId), eq(HashMap()), any())
        ).thenAnswer { it1 ->
            Observable.just(dataDetailMovieResponse).subscribe({
                it.body()?.apply {
                    (it1.arguments[2] as MovieTvSource.MovieDetailCallback).onSuccess(this)
                }
            }, {})
        }

        detailMovieViewModel.getDetail(sampleMovieId)
        Mockito.`when`(
            repository.isMovieExist(eq(sampleMovieId), any())
        ).thenAnswer { (it.arguments[1] as RequestResponse<Boolean>).success(true) }
        detailMovieViewModel.toogleFavorite()
        assertEquals(true, detailMovieViewModel.favorite.value)
    }

    @Test
    fun `Remove Favorite Movie`() {
        val dataDetailMovieResponse =
            apiService.getMovieDetails(sampleMovieId, mapListQueries).blockingSingle()

        Mockito.`when`(
            repository.getDataMovieDetails(eq(sampleMovieId), eq(HashMap()), any())
        ).thenAnswer { it1 ->
            Observable.just(dataDetailMovieResponse).subscribe({
                it.body()?.apply {
                    (it1.arguments[2] as MovieTvSource.MovieDetailCallback).onSuccess(this)
                }
            }, {})
        }

        detailMovieViewModel.getDetail(sampleMovieId)
        detailMovieViewModel.favorite.value = true
        Mockito.`when`(
            repository.isMovieExist(eq(sampleMovieId), any())
        ).thenAnswer { (it.arguments[1] as RequestResponse<Boolean>).success(false) }
        detailMovieViewModel.toogleFavorite()
        assertEquals(false, detailMovieViewModel.favorite.value)
    }


}