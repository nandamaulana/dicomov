package app.web.sleepcoderepeat.dicomov.data

import android.content.Context
import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import app.web.sleepcoderepeat.dicomov.BuildConfig
import app.web.sleepcoderepeat.dicomov.api.ApiService
import app.web.sleepcoderepeat.dicomov.data.local.LocalDataSource
import app.web.sleepcoderepeat.dicomov.data.local.entity.MovieWithGenre
import app.web.sleepcoderepeat.dicomov.data.local.entity.TvWithGenre
import app.web.sleepcoderepeat.dicomov.data.local.room.FavoriteDao
import app.web.sleepcoderepeat.dicomov.data.local.room.FavoriteDatabase
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvDataSource
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.RequestResponse
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.MovieItem
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.TvItem
import app.web.sleepcoderepeat.dicomov.data.remote.response.MovieDetailResponse
import app.web.sleepcoderepeat.dicomov.data.remote.response.MovieListResponse
import app.web.sleepcoderepeat.dicomov.data.remote.response.TvDetailResponse
import app.web.sleepcoderepeat.dicomov.data.remote.response.TvListResponse
import app.web.sleepcoderepeat.dicomov.utils.*
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.verify
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import retrofit2.Response
import javax.inject.Inject

@HiltAndroidTest
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P], application = HiltTestApplication::class)
class MovieTvRepositoryTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var apiService: ApiService

    @Mock
    lateinit var dataSource: MovieTvDataSource

    @Mock
    lateinit var localDataSource: LocalDataSource

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var movieTvRepository: MovieTvRepository

    private lateinit var dataMovieResponse: Response<MovieListResponse>
    private lateinit var dataMovieDetailResponse: Response<MovieDetailResponse>

    private lateinit var dataTvResponse: Response<TvListResponse>
    private lateinit var dataTvDetailResponse: Response<TvDetailResponse>

    private var dataFavoriteMovieWithGenreResponse = DummyData().generateDummyMovieWithGenres()
    private var dataFavoriteTvWithGenreResponse = DummyData().generateDummyTvWithGenres()

    private val sampleMovieId = 724989
    private val sampleTvId = 71712

    private val mapListQueries = HashMap<String?, String?>().apply {
        put(Constant.API_KEY, BuildConfig.API_TOKEN)
        put(Constant.PAGE_KEY, "1")
    }
    private val mapDetailQueries = HashMap<String?, String?>().apply {
        put(Constant.API_KEY, BuildConfig.API_TOKEN)
    }

    private lateinit var localDatabase: FavoriteDatabase
    private lateinit var favoriteDao: FavoriteDao
    private lateinit var context: Context

    @Before
    fun setUp() {
        hiltRule.inject()
        MockitoAnnotations.openMocks(this)
        movieTvRepository = MovieTvRepository(dataSource, localDataSource, appExecutors)
        dataMovieResponse = apiService.getDiscoverMovie(mapListQueries).blockingSingle()
        dataMovieDetailResponse =
            apiService.getMovieDetails(sampleMovieId, mapDetailQueries).blockingSingle()
        dataTvResponse = apiService.getDiscoverTv(mapListQueries).blockingSingle()
        dataTvDetailResponse =
            apiService.getTvDetails(sampleTvId, mapDetailQueries).blockingSingle()

        context = ApplicationProvider.getApplicationContext()

        localDatabase = Room.inMemoryDatabaseBuilder(
            context, FavoriteDatabase::class.java
        ).allowMainThreadQueries().build()
        favoriteDao = localDatabase.favoriteDao()
    }

    @After
    fun clear() {
        localDatabase.close()
    }

    @Test
    fun getDataDiscoverMovie() {
        `when`(dataSource.getDataDiscoverMovie(eq(mapListQueries), any())).thenAnswer {
            dataMovieResponse.body()?.apply {
                (it.arguments[1] as MovieTvSource.MovieListCallback).onSuccess(this)
            }
            null
        }

        val dataMovieList = MutableLiveData<MutableList<MovieItem>>()
        movieTvRepository.getDataDiscoverMovie(
            mapListQueries,
            object : MovieTvSource.MovieListCallback {
                override fun onSuccess(movieListResponse: MovieListResponse) {
                    val dataList: MutableList<MovieItem> = arrayListOf()
                    dataList.addAll(movieListResponse.movieListItems)
                    dataMovieList.postValue(dataList)
                }

                override fun onNotAvailable() {}

                override fun onError(msg: String?) {}
            })
        val dataMovieListResult = LiveDataTestUtil.getValue(dataMovieList)
        assertNotNull(dataMovieListResult)
        assertEquals(
            dataMovieResponse.body()?.movieListItems?.size?.toLong(),
            dataMovieListResult.size.toLong()
        )
    }

    @Test
    fun getDataMovieDetails() {
        `when`(
            dataSource.getDataMovieDetails(
                eq(sampleMovieId),
                eq(mapListQueries),
                any()
            )
        ).thenAnswer {
            dataMovieDetailResponse.body()?.apply {
                (it.arguments[2] as MovieTvSource.MovieDetailCallback).onSuccess(this)
            }
            null
        }

        val dataMovieResponse = MutableLiveData<MovieDetailResponse>()
        movieTvRepository.getDataMovieDetails(
            sampleMovieId,
            mapListQueries,
            object : MovieTvSource.MovieDetailCallback {
                override fun onSuccess(movieDetailResponse: MovieDetailResponse) {
                    dataMovieResponse.postValue(movieDetailResponse)
                }

                override fun onError(msg: String?) {}
            })
        val dataMovieResponseResult = LiveDataTestUtil.getValue(dataMovieResponse)
        assertNotNull(dataMovieResponseResult)
        assertEquals(
            dataMovieDetailResponse.body(),
            dataMovieResponseResult
        )
    }

    @Test
    fun getDataDiscoverTv() {
        `when`(dataSource.getDataDiscoverTv(eq(mapListQueries), any())).thenAnswer {
            dataTvResponse.body()?.apply {
                (it.arguments[1] as MovieTvSource.TvListCallback).onSuccess(this)
            }
            null
        }

        val dataTvList = MutableLiveData<MutableList<TvItem>>()
        movieTvRepository.getDataDiscoverTv(
            mapListQueries,
            object : MovieTvSource.TvListCallback {
                override fun onSuccess(tvListResponse: TvListResponse) {
                    val dataList: MutableList<TvItem> = arrayListOf()
                    dataList.addAll(tvListResponse.tvListItem)
                    dataTvList.postValue(dataList)
                }

                override fun onNotAvailable() {}

                override fun onError(msg: String?) {}
            })
        val dataTvListResult = LiveDataTestUtil.getValue(dataTvList)
        assertNotNull(dataTvListResult)
        assertEquals(
            dataTvResponse.body()?.tvListItem?.size?.toLong(),
            dataTvListResult.size.toLong()
        )
    }

    @Test
    fun getDataTvDetails() {
        `when`(
            dataSource.getDataTvDetails(
                eq(sampleTvId),
                eq(mapListQueries),
                any()
            )
        ).thenAnswer {
            dataTvDetailResponse.body()?.apply {
                (it.arguments[2] as MovieTvSource.TvDetailCallback).onSuccess(this)
            }
            null
        }

        val dataTvResponse = MutableLiveData<TvDetailResponse>()
        movieTvRepository.getDataTvDetails(
            sampleTvId,
            mapListQueries,
            object : MovieTvSource.TvDetailCallback {
                override fun onSuccess(tvDetailResponse: TvDetailResponse) {
                    dataTvResponse.postValue(tvDetailResponse)
                }

                override fun onError(msg: String?) {}
            })
        val dataTvResponseResult = LiveDataTestUtil.getValue(dataTvResponse)
        assertNotNull(dataTvResponseResult)
        assertEquals(
            dataTvDetailResponse.body(),
            dataTvResponseResult
        )
    }

    @Test
    fun getAllFavoritesMovie() {
        val dataSourceFactory =
            mock(DataSource.Factory::class.java) as DataSource.Factory<Int, MovieWithGenre>

        `when`(
            localDataSource.getListMovieWithGenres()
        ).thenReturn(dataSourceFactory)
        movieTvRepository.getAllFavoritesMovie()

        val movieWithGenresEntities =
            PagedListUtil.mockPagedList(DummyData().generateDummyMovieWithGenres())
        verify(localDataSource).getListMovieWithGenres()
        assertNotNull(movieWithGenresEntities)
        assertEquals(
            dataFavoriteMovieWithGenreResponse.size.toLong(),
            movieWithGenresEntities.size.toLong()
        )
    }

    @Test
    fun saveFavoriteMovie() {
        val favoriteMovie = dataFavoriteMovieWithGenreResponse[0]
        `when`(
            localDataSource.addFavoriteMovie(favoriteMovie.movie)
        ).thenReturn(favoriteMovie.movie.id.toLong())
        movieTvRepository.saveFavoriteMovie(favoriteMovie.movie, arrayListOf())
        Thread.sleep(1000L)

        verify(localDataSource).addFavoriteMovie(favoriteMovie.movie)
        val movieId = favoriteDao.addFavoriteMovie(favoriteMovie.movie)
        assertEquals(
            favoriteMovie.movie,
            LiveDataTestUtil.getValue(favoriteDao.getMovieByMovieId(movieId.toInt())).movie
        )
    }

    @Test
    fun deleteFavoriteMovieId() {
        val favoriteMovie = dataFavoriteMovieWithGenreResponse[0]
        `when`(
            localDataSource.addFavoriteMovie(favoriteMovie.movie)
        ).thenReturn(favoriteMovie.movie.id.toLong())
        movieTvRepository.saveFavoriteMovie(favoriteMovie.movie, arrayListOf())
        Thread.sleep(1000L)

        verify(localDataSource).addFavoriteMovie(favoriteMovie.movie)

        val movieId = favoriteDao.addFavoriteMovie(favoriteMovie.movie)
        assertEquals(
            favoriteMovie.movie,
            LiveDataTestUtil.getValue(favoriteDao.getMovieByMovieId(movieId.toInt())).movie
        )

        doAnswer {}.`when`(
            localDataSource
        ).removeFavoriteMovie(movieId.toInt())

        movieTvRepository.deleteFavoriteMovieId(movieId.toInt())

        verify(localDataSource).removeFavoriteMovie(movieId.toInt())
        favoriteDao.removeFavoriteByMovieId(movieId.toInt())
        assertEquals(false, favoriteDao.isMovieExistInFavorite(movieId.toInt()))
    }

    @Test
    fun isMovieExist() {
        val favoriteMovie = dataFavoriteMovieWithGenreResponse[0]
        `when`(
            localDataSource.addFavoriteMovie(favoriteMovie.movie)
        ).thenReturn(favoriteMovie.movie.id.toLong())
        movieTvRepository.saveFavoriteMovie(favoriteMovie.movie, arrayListOf())
        Thread.sleep(1000L)

        verify(localDataSource).addFavoriteMovie(favoriteMovie.movie)

        val movieId = favoriteDao.addFavoriteMovie(favoriteMovie.movie)
        assertEquals(
            favoriteMovie.movie,
            LiveDataTestUtil.getValue(favoriteDao.getMovieByMovieId(movieId.toInt())).movie
        )

        `when`(
            localDataSource.isMovieExistInFavorite(movieId.toInt())
        ).thenReturn(true)
        val favorite = MutableLiveData<Boolean>()
        movieTvRepository.isMovieExist(movieId.toInt(), object : RequestResponse<Boolean> {
            override fun success(t: Boolean) {
                favorite.postValue(t)
            }

            override fun fail() {}
        })
        val result = LiveDataTestUtil.getValue(favorite)
        assertNotNull(result)
        verify(localDataSource).isMovieExistInFavorite(movieId.toInt())
        assertEquals(true, result)
    }

    @Test
    fun getAllFavoritesTv() {
        val dataSourceFactory =
            mock(DataSource.Factory::class.java) as DataSource.Factory<Int, TvWithGenre>

        `when`(
            localDataSource.getListTvWithGenres()
        ).thenReturn(dataSourceFactory)
        movieTvRepository.getAllFavoritesTv()

        val tvWithGenreEntities =
            PagedListUtil.mockPagedList(DummyData().generateDummyTvWithGenres())
        verify(localDataSource).getListTvWithGenres()
        assertNotNull(tvWithGenreEntities)
        assertEquals(
            dataFavoriteTvWithGenreResponse.size.toLong(),
            tvWithGenreEntities.size.toLong()
        )
    }

    @Test
    fun saveFavoriteTv() {

        val favoriteTv = dataFavoriteTvWithGenreResponse[0]
        `when`(
            localDataSource.addFavoriteTv(favoriteTv.tv)
        ).thenReturn(favoriteTv.tv.id.toLong())

        movieTvRepository.saveFavoriteTv(favoriteTv.tv, arrayListOf())
        Thread.sleep(1000L)

        verify(localDataSource).addFavoriteTv(favoriteTv.tv)
        val tvId = favoriteDao.addFavoriteTv(favoriteTv.tv)
        assertEquals(
            favoriteTv.tv,
            LiveDataTestUtil.getValue(favoriteDao.getTvByTvId(tvId.toInt())).tv
        )

    }

    @Test
    fun deleteFavoriteTvId() {
        val favoriteTv = dataFavoriteTvWithGenreResponse[0]
        `when`(
            localDataSource.addFavoriteTv(favoriteTv.tv)
        ).thenReturn(favoriteTv.tv.id.toLong())
        movieTvRepository.saveFavoriteTv(favoriteTv.tv, arrayListOf())
        Thread.sleep(1000L)

        verify(localDataSource).addFavoriteTv(favoriteTv.tv)

        val tvId = favoriteDao.addFavoriteTv(favoriteTv.tv)
        assertEquals(
            favoriteTv.tv,
            LiveDataTestUtil.getValue(favoriteDao.getTvByTvId(tvId.toInt())).tv
        )

        doAnswer {}.`when`(
            localDataSource
        ).removeFavoriteTv(tvId.toInt())

        movieTvRepository.deleteFavoriteTvId(tvId.toInt())
        Thread.sleep(1000L)
        verify(localDataSource).removeFavoriteTv(tvId.toInt())

        favoriteDao.removeFavoriteTvByTvId(tvId.toInt())
        assertEquals(false, favoriteDao.isTvShowExistInFavorite(tvId.toInt()))
    }

    @Test
    fun isTvExists() {
        val favoriteTv = dataFavoriteTvWithGenreResponse[0]
        `when`(
            localDataSource.addFavoriteTv(favoriteTv.tv)
        ).thenReturn(favoriteTv.tv.id.toLong())
        movieTvRepository.saveFavoriteTv(favoriteTv.tv, arrayListOf())
        Thread.sleep(1000L)

        verify(localDataSource).addFavoriteTv(favoriteTv.tv)
        val tvId = favoriteDao.addFavoriteTv(favoriteTv.tv)
        assertEquals(
            favoriteTv.tv,
            LiveDataTestUtil.getValue(favoriteDao.getTvByTvId(tvId.toInt())).tv
        )

        `when`(
            localDataSource.isTvShowExistInFavorite(tvId.toInt())
        ).thenReturn(true)
        val favorite = MutableLiveData<Boolean>()
        movieTvRepository.isTvExists(tvId.toInt(), object : RequestResponse<Boolean> {
            override fun success(t: Boolean) {
                favorite.postValue(t)
            }

            override fun fail() {}
        })
        val result = LiveDataTestUtil.getValue(favorite)
        assertNotNull(result)
        verify(localDataSource).isTvShowExistInFavorite(tvId.toInt())
        assertEquals(true, result)
    }

}