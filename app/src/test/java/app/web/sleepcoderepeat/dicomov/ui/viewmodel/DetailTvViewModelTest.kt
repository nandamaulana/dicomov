package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.web.sleepcoderepeat.dicomov.BuildConfig
import app.web.sleepcoderepeat.dicomov.api.ApiService
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.RequestResponse
import app.web.sleepcoderepeat.dicomov.utils.Constant
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import javax.inject.Inject

@HiltAndroidTest
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P], application = HiltTestApplication::class)
class DetailTvViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var apiService: ApiService

    private lateinit var detailTvViewModel: DetailTvViewModel

    @Mock
    lateinit var repository: MovieTvRepository

    private val sampleTvId = 82856

    private val mapListQueries = HashMap<String?, String?>().apply {
        put(Constant.API_KEY, BuildConfig.API_TOKEN)
    }

    @Before
    fun init() {
        hiltRule.inject()
        MockitoAnnotations.initMocks(this)
        detailTvViewModel = DetailTvViewModel(repository)
    }

    @Test
    fun `Get Detail Tv`() {
        val dataDetailTvResponse =
            apiService.getTvDetails(sampleTvId, mapListQueries).blockingSingle()

        Mockito.`when`(
            repository.getDataTvDetails(eq(sampleTvId), eq(HashMap()), any())
        ).thenAnswer { it1 ->
            Observable.just(dataDetailTvResponse).subscribe({
                it.body()?.apply {
                    (it1.arguments[2] as MovieTvSource.TvDetailCallback).onSuccess(this)
                }
            }, {})
        }

        detailTvViewModel.getDetail(sampleTvId)
        assertNotNull(detailTvViewModel.tvData.value)
        assertEquals(
            dataDetailTvResponse.body(),
            detailTvViewModel.tvData.value
        )

    }

    @Test
    fun `Add Favorite Tv`() {
        val dataDetailTvResponse =
            apiService.getTvDetails(sampleTvId, mapListQueries).blockingSingle()

        Mockito.`when`(
            repository.getDataTvDetails(eq(sampleTvId), eq(HashMap()), any())
        ).thenAnswer { it1 ->
            Observable.just(dataDetailTvResponse).subscribe({
                it.body()?.apply {
                    (it1.arguments[2] as MovieTvSource.TvDetailCallback).onSuccess(this)
                }
            }, {})
        }

        detailTvViewModel.getDetail(sampleTvId)
        Mockito.`when`(
            repository.isTvExists(eq(sampleTvId), any())
        ).thenAnswer { (it.arguments[1] as RequestResponse<Boolean>).success(true) }
        detailTvViewModel.toogleFavorite()
        assertEquals(true, detailTvViewModel.favorite.value)
    }

    @Test
    fun `Remove Favorite Tv`() {
        val dataDetailTvResponse =
            apiService.getTvDetails(sampleTvId, mapListQueries).blockingSingle()

        Mockito.`when`(
            repository.getDataTvDetails(eq(sampleTvId), eq(HashMap()), any())
        ).thenAnswer { it1 ->
            Observable.just(dataDetailTvResponse).subscribe({
                it.body()?.apply {
                    (it1.arguments[2] as MovieTvSource.TvDetailCallback).onSuccess(this)
                }
            }, {})
        }

        detailTvViewModel.getDetail(sampleTvId)
        detailTvViewModel.favorite.value = true
        Mockito.`when`(
            repository.isTvExists(eq(sampleTvId), any())
        ).thenAnswer { (it.arguments[1] as RequestResponse<Boolean>).success(false) }
        detailTvViewModel.toogleFavorite()
        assertEquals(false, detailTvViewModel.favorite.value)
    }
}