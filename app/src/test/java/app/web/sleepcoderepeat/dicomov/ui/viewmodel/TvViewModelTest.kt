package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.web.sleepcoderepeat.dicomov.BuildConfig
import app.web.sleepcoderepeat.dicomov.api.ApiService
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.format
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap


@HiltAndroidTest
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P], application = HiltTestApplication::class)
class TvViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var apiService: ApiService

    private lateinit var tvViewModel: TvViewModel

    @Mock
    lateinit var repository: MovieTvRepository

    private val mapListQueries = HashMap<String?, String?>().apply {
        put(Constant.API_KEY, BuildConfig.API_TOKEN)
    }

    private val mockMapListQueries = HashMap<String?, String?>().apply {
        put(Constant.PRIMARY_RELEASE_DATE_KEY, Calendar.getInstance().time.format())
        put(Constant.PAGE_KEY, "1")
        put(Constant.SORT_BY_KEY, Constant.POPULARITY_DESC)
    }

    @Before
    fun init() {
        hiltRule.inject()
        MockitoAnnotations.initMocks(this)
        tvViewModel = TvViewModel(repository)
    }

    //
    @Test
    fun `Get List TV With Data`() {
        val dataMovieResponse = apiService.getDiscoverTv(mapListQueries).blockingSingle()

        Mockito.`when`(repository.getDataDiscoverTv(eq(mockMapListQueries), any()))
            .thenAnswer { it1 ->
                Observable.just(dataMovieResponse)
                    .subscribe({
                        it.body()?.apply {
                            (it1.arguments[1] as MovieTvSource.TvListCallback).onSuccess(this)
                        }
                    }, {})
            }

        tvViewModel.getListTv()
//        assertNotNull(tvViewModel.tvList.value)
        assertEquals(
            dataMovieResponse.body()?.tvListItem?.size,
            tvViewModel.tvList.value?.size
        )

    }
}