package app.web.sleepcoderepeat.dicomov.utils

import app.web.sleepcoderepeat.dicomov.data.local.entity.MovieEntity
import app.web.sleepcoderepeat.dicomov.data.local.entity.MovieWithGenre
import app.web.sleepcoderepeat.dicomov.data.local.entity.TvEntity
import app.web.sleepcoderepeat.dicomov.data.local.entity.TvWithGenre

class DummyData {

    fun generateDummyMovieWithGenres(): List<MovieWithGenre> = arrayListOf<MovieWithGenre>().apply {
        add(
            MovieWithGenre(
                MovieEntity(
                    1, "poster_path1", "title1",
                    "spoken_languages1",
                    "overview1",
                    "videos1",
                    1.1,
                    1.1,
                    "release_date1"
                ),
                arrayListOf()
            )
        )
        add(
            MovieWithGenre(
                MovieEntity(
                    2, "poster_path2", "title2",
                    "spoken_languages2",
                    "overview2",
                    "videos2",
                    2.2,
                    2.2,
                    "release_date2"
                ),
                arrayListOf()
            )
        )
        add(
            MovieWithGenre(
                MovieEntity(
                    3, "poster_path3", "title3",
                    "spoken_languages3",
                    "overview3",
                    "videos3",
                    3.3,
                    3.3,
                    "release_date3"
                ),
                arrayListOf()
            )
        )
        add(
            MovieWithGenre(
                MovieEntity(
                    4, "poster_path4", "title4",
                    "spoken_languages4",
                    "overview4",
                    "videos4",
                    4.4,
                    4.4,
                    "release_date4"
                ),
                arrayListOf()
            )
        )
        add(
            MovieWithGenre(
                MovieEntity(
                    5, "poster_path5", "title5",
                    "spoken_languages5",
                    "overview5",
                    "videos5",
                    5.5,
                    5.5,
                    "release_date5"
                ),
                arrayListOf()
            )
        )
    }
    fun generateDummyTvWithGenres(): List<TvWithGenre> = arrayListOf<TvWithGenre>().apply {
        add(
            TvWithGenre(
                TvEntity(
                    1, "poster_path1", "title1",
                    "spoken_languages1",
                    "overview1",
                    1.1,
                    1.1,
                    "release_date1"
                ),
                arrayListOf()
            )
        )
        add(
            TvWithGenre(
                TvEntity(
                    2, "poster_path2", "title2",
                    "spoken_languages2",
                    "overview2",
                    2.2,
                    2.2,
                    "release_date2"
                ),
                arrayListOf()
            )
        )
        add(
            TvWithGenre(
                TvEntity(
                    3, "poster_path3", "title3",
                    "spoken_languages3",
                    "overview3",
                    3.3,
                    3.3,
                    "release_date3"
                ),
                arrayListOf()
            )
        )
        add(
            TvWithGenre(
                TvEntity(
                    4, "poster_path4", "title4",
                    "spoken_languages4",
                    "overview4",
                    4.4,
                    4.4,
                    "release_date4"
                ),
                arrayListOf()
            )
        )
        add(
            TvWithGenre(
                TvEntity(
                    5, "poster_path5", "title5",
                    "spoken_languages5",
                    "overview5",
                    5.5,
                    5.5,
                    "release_date5"
                ),
                arrayListOf()
            )
        )
    }
}