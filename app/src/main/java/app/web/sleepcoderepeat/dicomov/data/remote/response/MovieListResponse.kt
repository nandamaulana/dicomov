package app.web.sleepcoderepeat.dicomov.data.remote.response

import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.MovieItem
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class MovieListResponse(
    val page: Int,
    @SerializedName("results") val movieListItems: MutableList<MovieItem>,
    val total_pages: Int,
    val total_results: Int
) : Serializable