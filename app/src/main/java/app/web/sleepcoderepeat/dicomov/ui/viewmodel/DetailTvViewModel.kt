package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.local.entity.TvEntity
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.RequestResponse
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.data.remote.response.TvDetailResponse
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.getRuntimeFormatted
import app.web.sleepcoderepeat.dicomov.utils.toImageUrl
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@HiltViewModel
class DetailTvViewModel @Inject constructor(
    private val movieTvRepository: MovieTvRepository
) : ViewModel() {

    private val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    val loading = MutableLiveData<Boolean>()
    val favorite = MutableLiveData(false)
    val tvData = MutableLiveData<TvDetailResponse>()
    val genreList = MutableLiveData<MutableList<Genre>>()
    val message = MutableLiveData<String>()
    val sumRuntime = MutableLiveData<Int>()

    fun getDetail(id: Int, withVideo: Boolean = false) {
        loading.value = true
        movieTvRepository.getDataTvDetails(
            id,
            if (withVideo) HashMap<String?, String?>().apply {
                put(Constant.APPEND_TO_RESPONSE, "videos")
            } else HashMap(),
            object : MovieTvSource.TvDetailCallback {
                override fun onSuccess(tvDetailResponse: TvDetailResponse) {
                    tvDetailResponse.poster_path = tvDetailResponse.poster_path?.toImageUrl()
                    tvData.value = tvDetailResponse
                    sumRuntime.value = tvDetailResponse.episode_run_time?.sum()
                    tvDetailResponse.genres?.let {
                        genreList.value?.apply {
                            clear()
                            addAll(tvDetailResponse.genres)
                        }
                    }
                    loading.value = false
                }

                override fun onError(msg: String?) {
                    msg?.let {
                        message.value = msg
                    }
                    loading.value = false
                }
            }
        ).apply {
            if (!isDisposed) {
                disposable.add(this)
            }
        }
        getFavoriteStatus(id)
    }

    private fun getFavoriteStatus(id: Int) {
        movieTvRepository.isTvExists(id, object : RequestResponse<Boolean> {
            override fun success(t: Boolean) {
                favorite.postValue(t)
            }

            override fun fail() {
                message.postValue("Fail to get favorite data")
            }

        })
    }

    fun getRuntimeFormatted(runtime: Int): String {
        return runtime.getRuntimeFormatted()
    }

    fun toogleFavorite() {
        if (favorite.value == true) {
            tvData.value?.id?.let {
                movieTvRepository.deleteFavoriteTvId(it)
            }
        } else {
            val genreList = genreList.value?.toList()
            tvData.value?.apply {
                movieTvRepository.saveFavoriteTv(
                    TvEntity(
                        id!!,
                        poster_path!!,
                        original_name!!,
                        spoken_languages.toString(),
                        overview!!,
                        vote_average!!,
                        popularity!!,
                        first_air_date!!
                    ),
                    genreList ?: arrayListOf()
                )
            }
        }
        tvData.value?.id?.let {
            getFavoriteStatus(it)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}