package app.web.sleepcoderepeat.dicomov.ui.screen.detailmovie

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import app.web.sleepcoderepeat.dicomov.databinding.ActivityDetailMovieBinding
import app.web.sleepcoderepeat.dicomov.ui.adapter.GenreItemAdapter
import app.web.sleepcoderepeat.dicomov.ui.dialog.VideoDialog
import app.web.sleepcoderepeat.dicomov.ui.listener.FavoriteClickListener
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.DetailMovieViewModel
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.ID_KEY
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.TYPE_KEY
import app.web.sleepcoderepeat.dicomov.utils.EspressoIdlingResource
import app.web.sleepcoderepeat.dicomov.utils.obtainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailMovieActivity : AppCompatActivity(), FavoriteClickListener {

    private lateinit var viewBinding: ActivityDetailMovieBinding
    private lateinit var dataIntent: Intent
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityDetailMovieBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(DetailMovieViewModel::class.java)
        }
        viewBinding.lifecycleOwner = this
        setContentView(viewBinding.root)

        dataIntent = intent

        setUpObserver()
        setUpRecyclerView()
    }

    private fun setUpObserver() {
        viewBinding.vm?.apply {
            dataIntent.getStringExtra(TYPE_KEY)?.let {
                getDetail(dataIntent.getIntExtra(ID_KEY, 0),true)
            }
            message.observe(this@DetailMovieActivity, {
                Toast.makeText(this@DetailMovieActivity, it, Toast.LENGTH_LONG).show()
            })
        }

        viewBinding.action = this

        viewBinding.playTrailer.setOnClickListener {
            viewBinding.vm?.movieData?.value?.videos?.let {
                if (it.getAsJsonArray("results").size() == 0) return@let
                it.getAsJsonArray("results")?.get(0)?.asJsonObject?.get("key")?.asString?.apply {
                    EspressoIdlingResource.increment()
                    VideoDialog(this).show(supportFragmentManager, "Video Dialog")
                }
                return@setOnClickListener
            }
            Toast.makeText(this, "Sorry trailer not found!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setUpRecyclerView() {
        viewBinding.vm?.apply {
                viewBinding.rvMovieGenre.adapter = GenreItemAdapter(arrayListOf())
        }
    }

    override fun onFavoriteClick() {
        viewBinding.vm?.toogleFavorite()
    }
}