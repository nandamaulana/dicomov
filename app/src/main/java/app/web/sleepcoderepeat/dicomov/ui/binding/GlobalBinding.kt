@file:Suppress("DEPRECATION")

package app.web.sleepcoderepeat.dicomov.ui.binding

import android.view.View
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.BindingAdapter
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.data.local.entity.MovieWithGenre
import app.web.sleepcoderepeat.dicomov.data.local.entity.TvWithGenre
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.MovieItem
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.TvItem
import app.web.sleepcoderepeat.dicomov.ui.adapter.*
import app.web.sleepcoderepeat.dicomov.utils.load
import com.facebook.shimmer.ShimmerFrameLayout

object GlobalBinding {

    @BindingAdapter("loadImage")
    @JvmStatic
    fun setImage(imageView: ImageView, url: String?) {
        url?.let {
            imageView.load(it)
        }
    }

    @BindingAdapter("genreList")
    @JvmStatic
    fun setGenreList(recyclerView: RecyclerView, dataList: MutableList<Genre>?) {
        dataList?.let {
            (recyclerView.adapter as GenreItemAdapter).replaceData(it)
        }
    }

    @BindingAdapter("movieList")
    @JvmStatic
    fun setMovieList(recyclerView: RecyclerView, dataList: MutableList<MovieItem>?) {
        dataList?.let {
            if (it.size != 0)
                (recyclerView.adapter as MovieItemAdapter).replaceData(it)
        }
    }

    @BindingAdapter("tvList")
    @JvmStatic
    fun setTvList(recyclerView: RecyclerView, dataList: MutableList<TvItem>?) {
        dataList?.let {
            if (it.size != 0)
                (recyclerView.adapter as TvItemAdapter).replaceData(it)
        }
    }

    @BindingAdapter("favoriteMovieList")
    @JvmStatic
    fun setFavoriteMovieList(recyclerView: RecyclerView, dataList: PagedList<MovieWithGenre>?) {
        dataList?.let {
            if (it.size != 0)
                (recyclerView.adapter as FavoriteMovieAdapter).submitList(dataList)
        }
    }


    @BindingAdapter("isLoading")
    @JvmStatic
    fun setIsLoading(shimmerFrameLayout: ShimmerFrameLayout, isLoading: Boolean) {

        if (isLoading) {
            shimmerFrameLayout.startShimmer()
            return
        }
        shimmerFrameLayout.stopShimmer()
    }

    @BindingAdapter("favIcon")
    @JvmStatic
    fun setFavoriteIcon(view: View, isFavorite: Boolean) {
        if (isFavorite)
            view.background =
                AppCompatResources.getDrawable(view.context, R.drawable.ic_baseline_favorite_24)
        else
            view.background = AppCompatResources.getDrawable(
                view.context,
                R.drawable.ic_baseline_favorite_border_24
            )
    }

    @BindingAdapter("favoriteTvList")
    @JvmStatic
    fun setFavoriteTvList(recyclerView: RecyclerView, dataList: PagedList<TvWithGenre>?) {
        dataList?.let {
            if (it.size != 0)
                (recyclerView.adapter as FavoriteTvAdapter).submitList(dataList)
        }
    }
}