package app.web.sleepcoderepeat.dicomov.utils

class Constant {
    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val IMAGE_BASE_URL_W500 = "https://image.tmdb.org/t/p/w500"
        const val ID_KEY = "id"
        const val API_KEY = "api_key"
        const val MOVIE_KEY = "movie"
        const val TYPE_KEY = "type"
        const val TV_KEY = "tv"
        const val PAGE_KEY = "page"
        const val SORT_BY_KEY = "sort_by"
        const val APPEND_TO_RESPONSE = "append_to_response"

        const val POPULARITY_DESC = "popularity.desc"
        const val PRIMARY_RELEASE_DATE_KEY = "primary_release_date.lte"

        fun getGenreMap(): HashMap<Int, String> {
            val genreMap = HashMap<Int, String>()
            genreMap[28] = "Action"
            genreMap[12] = "Adventure"
            genreMap[16] = "Animation"
            genreMap[35] = "Comedy"
            genreMap[80] = "Crime"
            genreMap[99] = "Documentary"
            genreMap[18] = "Drama"
            genreMap[10751] = "Family"
            genreMap[14] = "Fantasy"
            genreMap[36] = "History"
            genreMap[27] = "Horror"
            genreMap[10402] = "Music"
            genreMap[9648] = "Mystery"
            genreMap[10749] = "Romance"
            genreMap[878] = "Science Fiction"
            genreMap[53] = "Thriller"
            genreMap[10752] = "War"
            genreMap[37] = "Western"
            genreMap[10770] = "TV Movie"
            return genreMap
        }
    }
}