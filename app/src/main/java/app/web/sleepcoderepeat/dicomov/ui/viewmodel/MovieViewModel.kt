package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.MovieItem
import app.web.sleepcoderepeat.dicomov.data.remote.response.MovieListResponse
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.PAGE_KEY
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.POPULARITY_DESC
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.PRIMARY_RELEASE_DATE_KEY
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.SORT_BY_KEY
import app.web.sleepcoderepeat.dicomov.utils.SingleLiveEvent
import app.web.sleepcoderepeat.dicomov.utils.format
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val movieTvRepository: MovieTvRepository,
) : ViewModel() {

    private val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    val movieList = MutableLiveData<MutableList<MovieItem>>()
    val message = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()
    val page = MutableLiveData<String>()
    val openDetailMovie = SingleLiveEvent<Int>()

    init {
        loading.value = false
        page.value = "1"
    }

    fun getListMovie() {
        loading.value = true
        movieTvRepository.getDataDiscoverMovie(
            HashMap<String?, String?>().apply {
                put(PAGE_KEY, page.value)
                put(PRIMARY_RELEASE_DATE_KEY, Calendar.getInstance().time.format())
                put(SORT_BY_KEY, POPULARITY_DESC)
            },
            object : MovieTvSource.MovieListCallback {
                override fun onSuccess(movieListResponse: MovieListResponse) {
                    movieList.value?.clear()
                    movieList.value = movieListResponse.movieListItems
                    loading.value = false
                }

                override fun onNotAvailable() {
                    message.value = NOT_AVAILABLE
                    loading.value = false
                }

                override fun onError(msg: String?) {
                    msg?.let {
                        message.value = msg
                    }
                    loading.value = false
                }
            }
        ).apply {
            if (!isDisposed) {
                disposable.add(this)
            }
        }
    }

    fun openDetailMovie(id: Int) {
        openDetailMovie.value = id
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    companion object {
        private const val NOT_AVAILABLE = "Data Tidak Tersedia"
    }
}