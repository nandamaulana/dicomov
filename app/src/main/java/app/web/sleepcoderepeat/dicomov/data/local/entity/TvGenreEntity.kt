package app.web.sleepcoderepeat.dicomov.data.local.entity

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index

@Entity(
    tableName = "tv_genres",
    primaryKeys = ["tv_id", "id"],
    foreignKeys = [
        ForeignKey(
            entity = MovieEntity::class,
            parentColumns = ["id"],
            childColumns = ["tv_id"],
            onDelete = ForeignKey.CASCADE
        ),
    ],
    indices = [Index(value = ["tv_id"]), Index(value = ["id"])]
)
data class TvGenreEntity(
    @NonNull
    @ColumnInfo(name = "tv_id")
    var tv_id: Int,
    @NonNull
    @ColumnInfo(name = "id")
    var id: Int,
    @ColumnInfo(name = "name")
    var name: String,
)