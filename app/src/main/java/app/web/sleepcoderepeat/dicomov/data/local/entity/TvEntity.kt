package app.web.sleepcoderepeat.dicomov.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import javax.annotation.Nonnull

@Entity(tableName = "tv_shows")
data class TvEntity(
    @PrimaryKey
    @Nonnull
    @ColumnInfo(name = "id")
    var id: Int,
    @ColumnInfo(name = "poster_path")
    var poster_path: String,
    @ColumnInfo(name = "original_name")
    var original_name: String,
    @ColumnInfo(name = "spoken_languages", typeAffinity = ColumnInfo.TEXT)
    var spoken_languages: String,
    @ColumnInfo(name = "overview")
    var overview: String,
    @ColumnInfo(name = "vote_average", typeAffinity = ColumnInfo.REAL)
    var vote_average: Double? = 0.0,
    @ColumnInfo(name = "popularity", typeAffinity = ColumnInfo.REAL)
    var popularity: Double? = 0.0,
    @ColumnInfo(name = "first_air_date")
    var first_air_date: String?,
)