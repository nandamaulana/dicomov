package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.local.entity.MovieEntity
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.RequestResponse
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.data.remote.response.MovieDetailResponse
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.APPEND_TO_RESPONSE
import app.web.sleepcoderepeat.dicomov.utils.getRuntimeFormatted
import app.web.sleepcoderepeat.dicomov.utils.toImageUrl
import com.google.gson.JsonArray
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(
    private val movieTvRepository: MovieTvRepository
) : ViewModel() {

    private val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    val loading = MutableLiveData<Boolean>()
    val favorite = MutableLiveData(false)
    val movieData = MutableLiveData<MovieDetailResponse>()
    val genreList = MutableLiveData<MutableList<Genre>>()
    val message = MutableLiveData<String>()

    fun getDetail(id: Int, withVideo: Boolean = false) {
        loading.value = true
        movieTvRepository.getDataMovieDetails(
            id,
            if (withVideo) HashMap<String?, String?>().apply {
                put(APPEND_TO_RESPONSE, "videos")
            } else HashMap(),
            object : MovieTvSource.MovieDetailCallback {
                override fun onSuccess(movieDetailResponse: MovieDetailResponse) {
                    movieDetailResponse.poster_path =
                        movieDetailResponse.poster_path?.toImageUrl()
                    movieData.value = movieDetailResponse
                    movieDetailResponse.genres?.let {
                        genreList.value = it
                    }
                    loading.value = false
                }

                override fun onError(msg: String?) {
                    msg?.let {
                        message.value = it
                    }
                    loading.value = false
                }
            }
        ).apply {
            if (!isDisposed) {
                disposable.add(this)
            }
        }
        getFavoriteStatus(id)
    }

    private fun getFavoriteStatus(id: Int) {
        movieTvRepository.isMovieExist(id, object : RequestResponse<Boolean> {
            override fun success(t: Boolean) {
                favorite.postValue(t)
            }

            override fun fail() {
                message.postValue("Fail to get favorite data")
            }

        })
    }

    fun getSpokenLanguages(spokenLanguages: JsonArray?): String {
        var result = ""
        spokenLanguages?.forEach { it ->
            result += it.asJsonObject.get("name").asString + ", "
        }
        return "Language : " + result.dropLast(2)
    }

    fun getRuntimeFormatted(runtime: Int): String {
        return runtime.getRuntimeFormatted()
    }

    fun toogleFavorite() {
        if (favorite.value == true) {
            movieData.value?.id?.let {
                movieTvRepository.deleteFavoriteMovieId(it)
            }
        } else {
            val genreList = genreList.value?.toList()
            movieData.value?.apply {
                movieTvRepository.saveFavoriteMovie(
                    MovieEntity(
                        id!!,
                        poster_path!!,
                        title!!,
                        spoken_languages.toString(),
                        overview!!,
                        videos.toString(),
                        vote_average!!,
                        popularity!!,
                        release_date!!
                    ),
                    genreList ?: arrayListOf()
                )
            }
        }
        movieData.value?.id?.let {
            getFavoriteStatus(it)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}