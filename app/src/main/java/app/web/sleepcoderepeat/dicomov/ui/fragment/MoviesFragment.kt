package app.web.sleepcoderepeat.dicomov.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import app.web.sleepcoderepeat.dicomov.databinding.FragmentMoviesBinding
import app.web.sleepcoderepeat.dicomov.ui.adapter.MovieItemAdapter
import app.web.sleepcoderepeat.dicomov.ui.screen.detailmovie.DetailMovieActivity
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.MovieViewModel
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.ID_KEY
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.MOVIE_KEY
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.TYPE_KEY
import app.web.sleepcoderepeat.dicomov.utils.obtainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoviesFragment : Fragment() {

    private lateinit var viewBinding: FragmentMoviesBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentMoviesBinding.inflate(inflater).apply {
            vm = (activity as AppCompatActivity).obtainViewModel(MovieViewModel::class.java)
        }
        viewBinding.lifecycleOwner = this
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObserver()
        setUpRecyclerView()
    }


    private fun setUpObserver() {
        viewBinding.vm?.apply {
            openDetailMovie.observe(requireActivity(), {
                val intent = Intent(requireActivity(), DetailMovieActivity::class.java)
                intent.putExtra(ID_KEY, it)
                intent.putExtra(TYPE_KEY, MOVIE_KEY)
                startActivity(intent)
            })
            message.observe(requireActivity(), {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            })
            movieList.observe(requireActivity(), {
                this.movieList.value?.let {
                    viewBinding.movieList = it
                }
            })
            getListMovie()
        }
    }

    private fun setUpRecyclerView() {
        viewBinding.vm?.let {
            viewBinding.rvMovie.adapter = MovieItemAdapter(arrayListOf(), it, requireActivity())
        }
    }
}