package app.web.sleepcoderepeat.dicomov.data.remote.dataclass

data class ProductionCountry(
    val iso_3166_1: String,
    val name: String
)