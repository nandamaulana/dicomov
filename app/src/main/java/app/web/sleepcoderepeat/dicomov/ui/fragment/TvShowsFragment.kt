package app.web.sleepcoderepeat.dicomov.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import app.web.sleepcoderepeat.dicomov.databinding.FragmentTvShowsBinding
import app.web.sleepcoderepeat.dicomov.ui.adapter.TvItemAdapter
import app.web.sleepcoderepeat.dicomov.ui.screen.detailtv.DetailTvActivity
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.TvViewModel
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.ID_KEY
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.TV_KEY
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.TYPE_KEY
import app.web.sleepcoderepeat.dicomov.utils.obtainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TvShowsFragment : Fragment() {

    private lateinit var viewBinding: FragmentTvShowsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentTvShowsBinding.inflate(inflater).apply {
            vm = (activity as AppCompatActivity).obtainViewModel(TvViewModel::class.java)
        }
        viewBinding.lifecycleOwner = this
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObserver()
        setUpRecyclerView()

    }

    private fun setUpObserver() {
        viewBinding.vm?.apply {
            openDetailTv.observe(requireActivity(), {
                val intent = Intent(requireActivity(), DetailTvActivity::class.java)
                intent.putExtra(ID_KEY, it)
                intent.putExtra(TYPE_KEY, TV_KEY)
                startActivity(intent)
            })
            message.observe(requireActivity(), {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            })
            tvList.observe(requireActivity(), {
                this.tvList.value?.let {
                    viewBinding.tvList = it
                }
            })
            getListTv()
        }
    }

    private fun setUpRecyclerView() {
        viewBinding.vm?.let {
            viewBinding.rvTv.adapter = TvItemAdapter(arrayListOf(), it, requireActivity())
        }
    }
}