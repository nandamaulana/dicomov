package app.web.sleepcoderepeat.dicomov.data.local.entity

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index

@Entity(
    tableName = "movie_genres",
    primaryKeys = ["movie_id", "id"],
    foreignKeys = [
        ForeignKey(
            entity = MovieEntity::class,
            parentColumns = ["id"],
            childColumns = ["movie_id"],
            onDelete = CASCADE
        ),
    ],
    indices = [Index(value = ["movie_id"]), Index(value = ["id"])]
)
data class MovieGenreEntity(
    @NonNull
    @ColumnInfo(name = "movie_id")
    var movie_id: Int,
    @NonNull
    @ColumnInfo(name = "id")
    var id: Int,
    @ColumnInfo(name = "name")
    var name: String,
)