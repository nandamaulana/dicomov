package app.web.sleepcoderepeat.dicomov.ui.screen.splashscreen

import android.content.Intent
import android.os.Bundle
import android.view.animation.Animation
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.databinding.ActivitySplashScreenBinding
import app.web.sleepcoderepeat.dicomov.ui.screen.main.MainActivity
import app.web.sleepcoderepeat.dicomov.utils.getAnim
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var topAnim: Animation
    private lateinit var bottomAnim: Animation
    private lateinit var viewBinding: ActivitySplashScreenBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivitySplashScreenBinding.inflate(layoutInflater)
        viewBinding.lifecycleOwner = this
        setContentView(viewBinding.root)

        initAnimation()
        GlobalScope.launch {
            startAnimation()
            delay(1500L)
            startActivity(
                Intent(this@SplashScreenActivity, MainActivity::class.java),
                ActivityOptionsCompat.makeCustomAnimation(
                    applicationContext,
                    android.R.anim.fade_in, android.R.anim.fade_out
                ).toBundle()
            )
            finish()
        }
    }

    private fun initAnimation() {
        topAnim = getAnim(R.anim.splash_top)
        bottomAnim = getAnim(R.anim.splash_bottom)
    }

    private fun startAnimation() {
        viewBinding.rlMovieIcon.startAnimation(topAnim)
        viewBinding.tvAppName.startAnimation(bottomAnim)
    }
}