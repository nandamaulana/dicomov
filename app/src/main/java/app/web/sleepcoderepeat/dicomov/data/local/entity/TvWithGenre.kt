package app.web.sleepcoderepeat.dicomov.data.local.entity

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Embedded
import androidx.room.Relation

data class TvWithGenre(
    @Embedded
    var tv: TvEntity,

    @Relation(parentColumn = "id", entityColumn = "tv_id")
    var tvGenres: List<TvGenreEntity>

){
    companion object{
        val DIFF_CALLBACK: DiffUtil.ItemCallback<TvWithGenre> = object : DiffUtil.ItemCallback<TvWithGenre>() {
            override fun areItemsTheSame(oldItem: TvWithGenre, newItem: TvWithGenre): Boolean {
                return oldItem.tv.id == newItem.tv.id
            }

            override fun areContentsTheSame(oldItem: TvWithGenre, newItem: TvWithGenre): Boolean {
                return oldItem == newItem
            }
        }
    }
}