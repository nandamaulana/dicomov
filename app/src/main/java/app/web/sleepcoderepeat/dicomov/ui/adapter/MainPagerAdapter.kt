package app.web.sleepcoderepeat.dicomov.ui.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class MainPagerAdapter(activity: AppCompatActivity) :
    FragmentStateAdapter(activity) {

    private val listNavigation: MutableList<Fragment> = arrayListOf()

    fun addNavigationFragment(fragment: Fragment) = listNavigation.add(fragment)
    override fun getItemCount(): Int = listNavigation.size

    override fun createFragment(position: Int): Fragment = listNavigation[position]

}