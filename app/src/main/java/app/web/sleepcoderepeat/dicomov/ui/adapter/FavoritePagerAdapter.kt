package app.web.sleepcoderepeat.dicomov.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.viewpager2.adapter.FragmentStateAdapter

class FavoritePagerAdapter(fragmentManager: FragmentManager, activity: FragmentActivity) :
    FragmentStateAdapter(fragmentManager,activity.lifecycle) {

    private val listTab: MutableList<Fragment> = arrayListOf()

    fun addNavigationFragment(fragment: Fragment) = listTab.add(fragment)
    override fun getItemCount(): Int = listTab.size

    override fun createFragment(position: Int): Fragment = listTab[position]
}