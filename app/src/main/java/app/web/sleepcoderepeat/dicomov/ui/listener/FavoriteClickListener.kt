package app.web.sleepcoderepeat.dicomov.ui.listener

interface FavoriteClickListener {
    fun onFavoriteClick()
}