package app.web.sleepcoderepeat.dicomov.data.local.room

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import app.web.sleepcoderepeat.dicomov.data.local.entity.*

@Dao
interface FavoriteDao {

    // movie
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addFavoriteMovie(movie: MovieEntity): Long

    @Query("DELETE FROM movies where id = :movieId")
    fun removeFavoriteByMovieId(movieId: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addGenresMovie(movieGenres: List<MovieGenreEntity>)

    @Transaction
    @Query("SELECT * FROM movies")
    fun getListMovieWithGenres(): DataSource.Factory<Int, MovieWithGenre>

    @Transaction
    @Query("SELECT * FROM movies where id = :movieId")
    fun getMovieByMovieId(movieId: Int): LiveData<MovieWithGenre>

    @Query("SELECT EXISTS(SELECT * FROM movies where id = :movieId)")
    fun isMovieExistInFavorite(movieId: Int): Boolean

    // tv
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addFavoriteTv(tv: TvEntity): Long

    @Query("DELETE FROM tv_shows where id = :tvId")
    fun removeFavoriteTvByTvId(tvId: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addGenresTv(tvGenres: List<TvGenreEntity>)

    @Transaction
    @Query("SELECT * FROM tv_shows")
    fun getListTvWithGenres(): DataSource.Factory<Int, TvWithGenre>

    @Transaction
    @Query("SELECT * FROM tv_shows where id = :tvId")
    fun getTvByTvId(tvId: Int): LiveData<TvWithGenre>

    @Query("SELECT EXISTS(SELECT * FROM tv_shows where id = :tvId)")
    fun isTvShowExistInFavorite(tvId: Int): Boolean
}