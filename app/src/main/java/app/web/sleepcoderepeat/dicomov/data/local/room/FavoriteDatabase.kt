package app.web.sleepcoderepeat.dicomov.data.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import app.web.sleepcoderepeat.dicomov.data.local.entity.MovieEntity
import app.web.sleepcoderepeat.dicomov.data.local.entity.MovieGenreEntity
import app.web.sleepcoderepeat.dicomov.data.local.entity.TvEntity
import app.web.sleepcoderepeat.dicomov.data.local.entity.TvGenreEntity

@Database(
    entities = [MovieGenreEntity::class, MovieEntity::class, TvGenreEntity::class, TvEntity::class],
    version = 2,
    exportSchema = false
)

abstract class FavoriteDatabase : RoomDatabase() {
    abstract fun favoriteDao(): FavoriteDao
}