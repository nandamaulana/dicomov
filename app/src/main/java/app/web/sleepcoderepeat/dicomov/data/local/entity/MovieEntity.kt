package app.web.sleepcoderepeat.dicomov.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import javax.annotation.Nonnull

@Entity(tableName = "movies")
data class MovieEntity(
    @PrimaryKey
    @Nonnull
    @ColumnInfo(name = "id")
    var id: Int,
    @ColumnInfo(name = "poster_path")
    var poster_path: String,
    @ColumnInfo(name = "title")
    var title: String,
    @ColumnInfo(name = "spoken_languages", typeAffinity = ColumnInfo.TEXT)
    var spoken_languages: String,
    @ColumnInfo(name = "overview")
    var overview: String,
    @ColumnInfo(name = "videos", typeAffinity = ColumnInfo.TEXT)
    var videos: String? = "",
    @ColumnInfo(name = "vote_average", typeAffinity = ColumnInfo.REAL)
    var vote_average: Double? = 0.0,
    @ColumnInfo(name = "popularity", typeAffinity = ColumnInfo.REAL)
    var popularity: Double? = 0.0,
    @ColumnInfo(name = "release_date")
    var release_date: String?,
)