package app.web.sleepcoderepeat.dicomov.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import app.web.sleepcoderepeat.dicomov.databinding.FragmentFavoriteBinding
import app.web.sleepcoderepeat.dicomov.ui.adapter.FavoritePagerAdapter
import com.google.android.material.tabs.TabLayoutMediator

class FavoriteFragment : Fragment(), LifecycleObserver {

    private lateinit var viewBinding: FragmentFavoriteBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        viewBinding = FragmentFavoriteBinding.inflate(inflater)
        viewBinding.lifecycleOwner = this
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.viewPager.adapter =
            FavoritePagerAdapter(childFragmentManager, requireActivity()).apply {
                addNavigationFragment(FavoriteMovieFragment())
                addNavigationFragment(FavoriteTvFragment())
            }

        viewBinding.viewPager.offscreenPageLimit = 2

        TabLayoutMediator(viewBinding.tablayout, viewBinding.viewPager) { tab, position ->
            tab.text = tabFavorite[position]
        }.attach()

    }

    companion object {
        val tabFavorite = arrayListOf(
            "Movie Fav.",
            "Tv Fav."
        )
    }

}