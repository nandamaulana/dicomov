package app.web.sleepcoderepeat.dicomov.data.local

import androidx.paging.DataSource
import app.web.sleepcoderepeat.dicomov.data.local.entity.*
import app.web.sleepcoderepeat.dicomov.data.local.room.FavoriteDao
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalDataSource @Inject constructor(
    private val favoriteDao: FavoriteDao
)  {

    //movie

    fun addFavoriteMovie(movie: MovieEntity) = favoriteDao.addFavoriteMovie(movie)

    fun removeFavoriteMovie(movieId: Int) {
        favoriteDao.removeFavoriteByMovieId(movieId)
    }

    fun addGenresMovie(movieGenres: List<MovieGenreEntity>) = favoriteDao.addGenresMovie(movieGenres)

//    fun removeGenresByMovie(movieId: Int) = favoriteDao.removeGenresByMovieId(movieId)

    fun getListMovieWithGenres(): DataSource.Factory<Int, MovieWithGenre> = favoriteDao.getListMovieWithGenres()

//    fun getMovieByMovieId(movieId: Int): LiveData<MovieWithGenre> = favoriteDao.getMovieByMovieId(movieId)

    fun isMovieExistInFavorite(movieId: Int): Boolean = favoriteDao.isMovieExistInFavorite(movieId)

    //tv

    fun addFavoriteTv(tv: TvEntity) = favoriteDao.addFavoriteTv(tv)

    fun removeFavoriteTv(tvId: Int) = favoriteDao.removeFavoriteTvByTvId(tvId)

    fun addGenresTv(tvGenres: List<TvGenreEntity>) = favoriteDao.addGenresTv(tvGenres)

//    fun removeGenresByTvId(tvId: Int) = favoriteDao.removeGenresByTvId(tvId)

    fun getListTvWithGenres(): DataSource.Factory<Int, TvWithGenre> = favoriteDao.getListTvWithGenres()

//    fun getTvByTvId(tvID: Int): LiveData<TvWithGenre> = favoriteDao.getTvByTvId(tvID)

    fun isTvShowExistInFavorite(tvId: Int): Boolean = favoriteDao.isTvShowExistInFavorite(tvId)

}