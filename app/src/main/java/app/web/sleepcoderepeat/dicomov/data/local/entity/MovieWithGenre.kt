package app.web.sleepcoderepeat.dicomov.data.local.entity

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Embedded
import androidx.room.Relation

data class MovieWithGenre(
    @Embedded
    var movie: MovieEntity,

    @Relation(parentColumn = "id", entityColumn = "movie_id")
    var movieGenres: List<MovieGenreEntity>

){
    companion object{
        val DIFF_CALLBACK: DiffUtil.ItemCallback<MovieWithGenre> = object : DiffUtil.ItemCallback<MovieWithGenre>() {
            override fun areItemsTheSame(oldItem: MovieWithGenre, newItem: MovieWithGenre): Boolean {
                return oldItem.movie.id == newItem.movie.id
            }

            override fun areContentsTheSame(oldItem: MovieWithGenre, newItem: MovieWithGenre): Boolean {
                return oldItem == newItem
            }
        }
    }
}