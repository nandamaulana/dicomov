package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.local.entity.TvWithGenre
import app.web.sleepcoderepeat.dicomov.utils.EspressoIdlingResource
import app.web.sleepcoderepeat.dicomov.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FavoriteTvViewModel @Inject constructor(
    private val movieTvRepository: MovieTvRepository
): ViewModel() {

    val openDetailTv = SingleLiveEvent<Int>()

    @Suppress("DEPRECATION")
    fun getListFavorite(): LiveData<PagedList<TvWithGenre>> {
        EspressoIdlingResource.increment()
        return movieTvRepository.getAllFavoritesTv()
    }

    fun openDetailTv(id: Int) {
        openDetailTv.value = id
    }
}