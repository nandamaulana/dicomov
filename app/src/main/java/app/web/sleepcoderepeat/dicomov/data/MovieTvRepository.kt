package app.web.sleepcoderepeat.dicomov.data

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import app.web.sleepcoderepeat.dicomov.data.local.LocalDataSource
import app.web.sleepcoderepeat.dicomov.data.local.entity.*
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvDataSource
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.RequestResponse
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.utils.AppExecutors
import app.web.sleepcoderepeat.dicomov.utils.EspressoIdlingResource
import io.reactivex.disposables.Disposable
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton


@Suppress("DEPRECATION")
@Singleton
class MovieTvRepository @Inject constructor(
    private val movieTvDataSource: MovieTvDataSource,
    private val localDataSource: LocalDataSource,
    private val appExecutors: AppExecutors
) : MovieTvSource {

    override fun getDataDiscoverMovie(
        map: HashMap<String?, String?>,
        callback: MovieTvSource.MovieListCallback
    ): Disposable = movieTvDataSource.getDataDiscoverMovie(map, callback)

    override fun getDataMovieDetails(
        movieId: Int,
        map: HashMap<String?, String?>,
        callback: MovieTvSource.MovieDetailCallback
    ): Disposable = movieTvDataSource.getDataMovieDetails(movieId, map, callback)

    override fun getDataDiscoverTv(
        map: HashMap<String?, String?>,
        callback: MovieTvSource.TvListCallback
    ): Disposable = movieTvDataSource.getDataDiscoverTv(map, callback)

    override fun getDataTvDetails(
        tvId: Int,
        map: HashMap<String?, String?>,
        callback: MovieTvSource.TvDetailCallback
    ): Disposable = movieTvDataSource.getDataTvDetails(tvId, map, callback)

    fun getAllFavoritesMovie(): LiveData<PagedList<MovieWithGenre>> {

        val pagedListConfig: PagedList.Config = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setPrefetchDistance(10)
            .setPageSize(20).build()

        return LivePagedListBuilder(
            localDataSource.getListMovieWithGenres(),
            pagedListConfig
        ).build()
    }

    fun saveFavoriteMovie(movieEntity: MovieEntity, listGenre: List<Genre>) {
        EspressoIdlingResource.increment()
        appExecutors.diskIO().execute {
            val movieId = localDataSource.addFavoriteMovie(movieEntity)

            val listMovieGenres = listGenre.map {
                it.id?.let { it1 ->
                    it.name?.let { it2 ->
                        return@map MovieGenreEntity(
                            movieId.toInt(), it1,
                            it2
                        )
                    }
                }
            }.toList()

            localDataSource.addGenresMovie(listMovieGenres.filterNotNull())
            EspressoIdlingResource.decrement()
        }
    }

    fun deleteFavoriteMovieId(movieId: Int) {
        appExecutors.diskIO().execute {
            localDataSource.removeFavoriteMovie(movieId)
        }
    }

    fun isMovieExist(movieId: Int, requestResponse: RequestResponse<Boolean>) {
        EspressoIdlingResource.increment()
        try {
            appExecutors.diskIO().execute {
                requestResponse.success(localDataSource.isMovieExistInFavorite(movieId))
                EspressoIdlingResource.decrement()
            }
        }catch (e: Exception){
            requestResponse.fail()
            EspressoIdlingResource.decrement()
        }
    }

    fun getAllFavoritesTv(): LiveData<PagedList<TvWithGenre>> {

        val pagedListConfig: PagedList.Config = PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setPrefetchDistance(10)
            .setPageSize(20).build()

        return LivePagedListBuilder(
            localDataSource.getListTvWithGenres(),
            pagedListConfig
        ).build()
    }


    fun saveFavoriteTv(tvEntity: TvEntity, listGenre: List<Genre>) {
        appExecutors.diskIO().execute {
            val tvId = localDataSource.addFavoriteTv(tvEntity)

            val listMovieGenres = listGenre.map {
                it.id?.let { it1 ->
                    it.name?.let { it2 ->
                        return@map TvGenreEntity(
                            tvId.toInt(), it1,
                            it2
                        )
                    }
                }
            }.toList()

            localDataSource.addGenresTv(listMovieGenres.filterNotNull())
        }
    }

    fun deleteFavoriteTvId(tvId: Int) {
        appExecutors.diskIO().execute {
            localDataSource.removeFavoriteTv(tvId)
        }
    }

    fun isTvExists(tvId: Int, requestResponse: RequestResponse<Boolean>) {
        appExecutors.diskIO().execute {
            requestResponse.success(localDataSource.isTvShowExistInFavorite(tvId))
        }
    }

}