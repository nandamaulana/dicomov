package app.web.sleepcoderepeat.dicomov.data.remote

import app.web.sleepcoderepeat.dicomov.BuildConfig
import app.web.sleepcoderepeat.dicomov.api.ApiService
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.API_KEY
import app.web.sleepcoderepeat.dicomov.utils.EspressoIdlingResource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieTvDataSource @Inject constructor(
    private val apiService: ApiService
) : MovieTvSource {

    override fun getDataDiscoverMovie(
        map: HashMap<String?, String?>,
        callback: MovieTvSource.MovieListCallback
    ): Disposable {
        EspressoIdlingResource.increment()
        return apiService.getDiscoverMovie(
            map.apply { put(API_KEY, BuildConfig.API_TOKEN) }
        ).observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.code() != CODE_200) {
                    callback.onError(it.errorBody().toString())
                } else {
                    it.body()?.apply {
                        if (this.movieListItems.size == 0) {
                            callback.onNotAvailable()
                        } else {
                            callback.onSuccess(this)
                        }
                    }
                }
                EspressoIdlingResource.decrement()
            }, {
                callback.onError(it.localizedMessage)
                EspressoIdlingResource.decrement()
            })
    }

    override fun getDataMovieDetails(
        movieId: Int,
        map: HashMap<String?, String?>,
        callback: MovieTvSource.MovieDetailCallback
    ): Disposable {
        EspressoIdlingResource.increment()
        return apiService.getMovieDetails(
            movieId,
            map.apply { put(API_KEY, BuildConfig.API_TOKEN) })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.code() != CODE_200) {
                    callback.onError(it.errorBody().toString())
                } else {
                    it.body()?.apply {
                        print(this.toString())
                        callback.onSuccess(this)
                    }
                }
                EspressoIdlingResource.decrement()
            }, {
                callback.onError(it.localizedMessage)
                EspressoIdlingResource.decrement()
            })
    }

    override fun getDataDiscoverTv(
        map: HashMap<String?, String?>,
        callback: MovieTvSource.TvListCallback
    ): Disposable {
        EspressoIdlingResource.increment()
        return apiService.getDiscoverTv(map.apply { put(API_KEY, BuildConfig.API_TOKEN) })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.code() != CODE_200) {
                    callback.onError(it.errorBody().toString())
                } else {
                    it.body()?.apply {
                        if (tvListItem.size == 0) {
                            callback.onNotAvailable()
                        } else {
                            callback.onSuccess(this)
                        }
                    }
                }
                EspressoIdlingResource.decrement()
            }, {
                callback.onError(it.localizedMessage)
                EspressoIdlingResource.decrement()
            })
    }

    override fun getDataTvDetails(
        tvId: Int,
        map: HashMap<String?, String?>,
        callback: MovieTvSource.TvDetailCallback
    ): Disposable {
        EspressoIdlingResource.increment()
        return apiService.getTvDetails(tvId, map.apply { put(API_KEY, BuildConfig.API_TOKEN) })
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it.code() != CODE_200) {
                    callback.onError(it.errorBody().toString())
                } else {
                    it.body()?.apply {
                        callback.onSuccess(this)
                    }
                }
                EspressoIdlingResource.decrement()
            }, {
                callback.onError(it.localizedMessage)
                EspressoIdlingResource.decrement()
            })
    }

    companion object {
        private const val CODE_200 = 200
    }
}