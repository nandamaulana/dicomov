package app.web.sleepcoderepeat.dicomov.ui.adapter

import android.app.Activity
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.data.local.entity.MovieWithGenre
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.databinding.FavoriteMovieItemBinding
import app.web.sleepcoderepeat.dicomov.ui.listener.ItemClickListener
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.FavoriteMovieViewModel
import app.web.sleepcoderepeat.dicomov.utils.EspressoIdlingResource
import app.web.sleepcoderepeat.dicomov.utils.getSizeByScreenSize
import app.web.sleepcoderepeat.dicomov.utils.toImageUrl

class FavoriteMovieAdapter constructor(
    private val viewModel: FavoriteMovieViewModel,
    val context: Activity
) : PagedListAdapter<MovieWithGenre, FavoriteMovieAdapter.FavoriteViewHolder>(MovieWithGenre.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        return FavoriteViewHolder(
            context,
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.favorite_movie_item,
                parent,
                false
            )
        )
    }

    override fun submitList(pagedList: PagedList<MovieWithGenre>?) {
        super.submitList(pagedList)
        EspressoIdlingResource.decrement()
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, object : ItemClickListener {
                override fun onItemClick() {
                    viewModel.openDetailMovie(it.movie.id)
                }
            })
        }
    }

    class FavoriteViewHolder(val context: Activity, val binding: FavoriteMovieItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(movieWithGenre: MovieWithGenre, onItemCLick: ItemClickListener) {
            val movie = movieWithGenre.movie.copy()
            movieWithGenre.movie.poster_path.let {
                movie.poster_path = it.toImageUrl()
            }
            binding.movieImage.clipToOutline = true
            binding.movieItem = movie
            val genreList: List<Genre> = movieWithGenre.movieGenres.map {
                return@map Genre(it.id, it.name)
            }.toList()

            val dm = DisplayMetrics()
            context.windowManager.defaultDisplay.getMetrics(dm)
            val sized = dm.getSizeByScreenSize()
            binding.movieItemLayout.apply {
                layoutParams.width = sized
                layoutParams.height = sized + (sized / 2)
            }
            binding.rvMovieGenre.adapter = GenreItemAdapter(genreList.toMutableList())
            binding.action = onItemCLick
            binding.executePendingBindings()
        }
    }
}