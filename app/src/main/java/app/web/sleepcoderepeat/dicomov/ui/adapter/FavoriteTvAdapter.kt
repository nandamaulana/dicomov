package app.web.sleepcoderepeat.dicomov.ui.adapter

import android.app.Activity
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.data.local.entity.TvWithGenre
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.databinding.FavoriteTvItemBinding
import app.web.sleepcoderepeat.dicomov.ui.listener.ItemClickListener
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.FavoriteTvViewModel
import app.web.sleepcoderepeat.dicomov.utils.EspressoIdlingResource
import app.web.sleepcoderepeat.dicomov.utils.getSizeByScreenSize
import app.web.sleepcoderepeat.dicomov.utils.toImageUrl

class FavoriteTvAdapter(
    private val viewModel: FavoriteTvViewModel,
    val context: Activity
) : PagedListAdapter<TvWithGenre, FavoriteTvAdapter.FavoriteViewHolder>(TvWithGenre.DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        return FavoriteViewHolder(
            context,
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.favorite_tv_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, object : ItemClickListener {
                override fun onItemClick() {
                    viewModel.openDetailTv(it.tv.id)
                }

            })
        }
        EspressoIdlingResource.decrement()
    }

    class FavoriteViewHolder(val context: Activity, val binding: FavoriteTvItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(tvWithGenre: TvWithGenre, onItemClick: ItemClickListener) {
            val tv = tvWithGenre.tv.copy()
            tvWithGenre.tv.poster_path.let {
                tv.poster_path = it.toImageUrl()
            }
            binding.tvImage.clipToOutline = true
            binding.tvItem = tv
            val genreList: List<Genre> = tvWithGenre.tvGenres.map {
                return@map Genre(it.id, it.name)
            }.toList()

            val dm = DisplayMetrics()
            context.windowManager.defaultDisplay.getMetrics(dm)
            val sized = dm.getSizeByScreenSize()
            binding.tvItemLayout.apply {
                layoutParams.width = sized
                layoutParams.height = sized + (sized / 2)
            }
            binding.rvTvGenre.adapter = GenreItemAdapter(genreList.toMutableList())
            binding.action = onItemClick
            binding.executePendingBindings()
        }
    }
}