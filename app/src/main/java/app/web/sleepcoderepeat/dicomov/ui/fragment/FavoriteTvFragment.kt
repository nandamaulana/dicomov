package app.web.sleepcoderepeat.dicomov.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import app.web.sleepcoderepeat.dicomov.databinding.FragmentFavoriteTvBinding
import app.web.sleepcoderepeat.dicomov.ui.adapter.FavoriteTvAdapter
import app.web.sleepcoderepeat.dicomov.ui.screen.detailtv.DetailTvActivity
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.FavoriteTvViewModel
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.obtainViewModel

class FavoriteTvFragment : Fragment() {
    private lateinit var viewBinding: FragmentFavoriteTvBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentFavoriteTvBinding.inflate(inflater).apply {
            vm = (activity as AppCompatActivity).obtainViewModel(FavoriteTvViewModel::class.java)
        }
        viewBinding.lifecycleOwner = this
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObserver()
        setUpRecyclerView()
    }


    private fun setUpObserver() {
        viewBinding.vm?.apply {
            getListFavorite().observe(viewLifecycleOwner) {
                (viewBinding.rvFavoriteTv.adapter as FavoriteTvAdapter).submitList(it)
            }
            openDetailTv.observe(requireActivity(), {
                val intent = Intent(requireActivity(), DetailTvActivity::class.java)
                intent.putExtra(Constant.ID_KEY, it)
                intent.putExtra(Constant.TYPE_KEY, Constant.TV_KEY)
                startActivity(intent)
            })
        }
    }

    private fun setUpRecyclerView() {
        viewBinding.vm?.let {
            viewBinding.rvFavoriteTv.adapter = FavoriteTvAdapter(it, requireActivity())
        }
    }
}