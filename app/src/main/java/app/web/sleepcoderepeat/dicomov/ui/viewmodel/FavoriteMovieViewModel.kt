package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.local.entity.MovieWithGenre
import app.web.sleepcoderepeat.dicomov.utils.EspressoIdlingResource
import app.web.sleepcoderepeat.dicomov.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FavoriteMovieViewModel @Inject constructor(
    private val movieTvRepository: MovieTvRepository
): ViewModel() {

    val openDetailMovie = SingleLiveEvent<Int>()

    @Suppress("DEPRECATION")
    fun getListFavorite(): LiveData<PagedList<MovieWithGenre>> {
        EspressoIdlingResource.increment()
        return movieTvRepository.getAllFavoritesMovie()
    }

    fun openDetailMovie(id: Int) {
        openDetailMovie.value = id
    }
}