package app.web.sleepcoderepeat.dicomov.ui.listener

interface ItemClickListener {
    fun onItemClick()
}