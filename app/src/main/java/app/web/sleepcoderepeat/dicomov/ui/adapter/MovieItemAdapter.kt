package app.web.sleepcoderepeat.dicomov.ui.adapter

import android.app.Activity
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.MovieItem
import app.web.sleepcoderepeat.dicomov.databinding.MovieItemBinding
import app.web.sleepcoderepeat.dicomov.ui.listener.ItemClickListener
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.MovieViewModel
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.getGenreMap
import app.web.sleepcoderepeat.dicomov.utils.getSizeByScreenSize
import app.web.sleepcoderepeat.dicomov.utils.toImageUrl

class MovieItemAdapter constructor(
    private var movieItemList: MutableList<MovieItem>,
    private val viewModel: MovieViewModel,
    val context: Activity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MovieItemHolder(
            context,
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.movie_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        movieItemList[position].apply {
            (holder as MovieItemHolder).bind(this, object : ItemClickListener {
                override fun onItemClick() {
                    id?.let {
                        viewModel.openDetailMovie(it)
                    }
                }
            })
        }
    }

    override fun getItemCount(): Int = movieItemList.size

    fun replaceData(movieList: MutableList<MovieItem>) {
        setList(movieList)
    }

    private fun setList(movieList: MutableList<MovieItem>) {
        this.movieItemList = movieList
        notifyDataSetChanged()
    }

    class MovieItemHolder constructor(
        val context: Activity,
        val binding: MovieItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(movieItem: MovieItem, mainActionListener: ItemClickListener) {
            val item = movieItem.copy()
            movieItem.poster_path?.let {
                item.poster_path = movieItem.poster_path?.toImageUrl()
            }
            binding.movieImage.clipToOutline = true
            binding.movieItem = item
            val genreList: MutableList<Genre> = arrayListOf()
            val genreMap = getGenreMap()
            movieItem.genre_ids?.apply {
                map { i ->
                    if (!genreMap[i].isNullOrBlank())
                        genreList.add(Genre(i, genreMap[i]))
                }
            }
            val dm = DisplayMetrics()
            context.windowManager.defaultDisplay.getMetrics(dm)
            val sized = dm.getSizeByScreenSize()
            binding.movieItemLayout.apply {
                layoutParams.width = sized
                layoutParams.height = sized + (sized / 2)
            }
            binding.rvMovieGenre.adapter = GenreItemAdapter(genreList)
            binding.action = mainActionListener
            binding.executePendingBindings()
        }

    }
}