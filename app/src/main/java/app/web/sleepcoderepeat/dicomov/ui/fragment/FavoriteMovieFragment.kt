package app.web.sleepcoderepeat.dicomov.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import app.web.sleepcoderepeat.dicomov.databinding.FragmentFavoriteMovieBinding
import app.web.sleepcoderepeat.dicomov.ui.adapter.FavoriteMovieAdapter
import app.web.sleepcoderepeat.dicomov.ui.screen.detailmovie.DetailMovieActivity
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.FavoriteMovieViewModel
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.obtainViewModel

class FavoriteMovieFragment : Fragment() {
    private lateinit var viewBinding: FragmentFavoriteMovieBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentFavoriteMovieBinding.inflate(inflater).apply {
            vm = (activity as AppCompatActivity).obtainViewModel(FavoriteMovieViewModel::class.java)
        }
        viewBinding.lifecycleOwner = this
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObserver()
        setUpRecyclerView()
    }


    private fun setUpObserver() {
        viewBinding.vm?.apply {
            getListFavorite().observe(viewLifecycleOwner) {
                (viewBinding.rvFavoriteMovie.adapter as FavoriteMovieAdapter).submitList(it)
            }
            openDetailMovie.observe(viewLifecycleOwner){
                val intent = Intent(requireActivity(), DetailMovieActivity::class.java)
                intent.putExtra(Constant.ID_KEY, it)
                intent.putExtra(Constant.TYPE_KEY, Constant.MOVIE_KEY)
                startActivity(intent)
            }
        }
    }

    private fun setUpRecyclerView() {
        viewBinding.vm?.let {
            viewBinding.rvFavoriteMovie.adapter = FavoriteMovieAdapter(it, requireActivity())
        }
    }
}