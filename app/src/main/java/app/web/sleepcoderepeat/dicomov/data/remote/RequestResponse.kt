package app.web.sleepcoderepeat.dicomov.data.remote

interface RequestResponse<T> {
    fun success(t: T)
    fun fail()
}