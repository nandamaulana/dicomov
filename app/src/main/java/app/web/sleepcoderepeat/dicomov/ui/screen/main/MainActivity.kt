package app.web.sleepcoderepeat.dicomov.ui.screen.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.forEach
import androidx.viewpager2.widget.ViewPager2
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.databinding.ActivityMainBinding
import app.web.sleepcoderepeat.dicomov.ui.adapter.MainPagerAdapter
import app.web.sleepcoderepeat.dicomov.ui.fragment.FavoriteFragment
import app.web.sleepcoderepeat.dicomov.ui.fragment.MoviesFragment
import app.web.sleepcoderepeat.dicomov.ui.fragment.TvShowsFragment
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var viewBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        viewBinding.lifecycleOwner = this
        setContentView(viewBinding.root)


        setupBottomNavigation()
    }

    private fun setupBottomNavigation() {
        viewBinding.viewPager.adapter = MainPagerAdapter(this).apply {
            addNavigationFragment(MoviesFragment())
            addNavigationFragment(TvShowsFragment())
            addNavigationFragment(FavoriteFragment())
        }
        viewBinding.viewPager.offscreenPageLimit = 2

        viewBinding.bottomNav.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.action_movies -> viewBinding.viewPager.currentItem = 0
                R.id.action_tv -> viewBinding.viewPager.currentItem = 1
                R.id.favorite -> viewBinding.viewPager.currentItem = 2
            }
            false
        }

        viewBinding.viewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback(){
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                viewBinding.bottomNav.menu.forEach {
                    it.isChecked = false
                }
                viewBinding.bottomNav.menu.getItem(position).isChecked = true
            }
        })

    }

}