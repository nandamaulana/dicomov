package app.web.sleepcoderepeat.dicomov.data.remote.dataclass

data class TvItem(
    val backdrop_path: String?,
    val first_air_date: String?,
    val genre_ids: List<Int>?,
    val id: Int?,
    val name: String?,
    val origin_country: List<String>?,
    val original_language: String?,
    val original_name: String?,
    val overview: String?,
    val popularity: Double?,
    var poster_path: String?,
    val vote_average: Double?,
    val vote_count: Int?,
)