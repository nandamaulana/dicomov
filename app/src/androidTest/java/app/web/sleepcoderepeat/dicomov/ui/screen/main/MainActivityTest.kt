package app.web.sleepcoderepeat.dicomov.ui.screen.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.PerformException
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import app.web.sleepcoderepeat.dicomov.BuildConfig
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.api.ApiService
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.MovieItem
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.TvItem
import app.web.sleepcoderepeat.dicomov.ui.screen.splashscreen.SplashScreenActivity
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.EspressoIdlingResource
import app.web.sleepcoderepeat.dicomov.utils.format
import com.google.android.material.tabs.TabLayout
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.hamcrest.core.AllOf.allOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap

@HiltAndroidTest
class MainActivityTest {

    private lateinit var listMovie: MutableList<MovieItem>
    private lateinit var listTv: MutableList<TvItem>
    private val totalFavoriteMovie = 10
    private val totalFavoriteTv = 10

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var apiService: ApiService

    private val mapListQueries = HashMap<String?, String?>().apply {
        put(Constant.API_KEY, BuildConfig.API_TOKEN)
        put(Constant.PAGE_KEY, "1")
        put(Constant.PRIMARY_RELEASE_DATE_KEY, Calendar.getInstance().time.format())
        put(Constant.SORT_BY_KEY, Constant.POPULARITY_DESC)
    }

    @Rule
    @JvmField
    var activityTestRule = ActivityScenarioRule(SplashScreenActivity::class.java)

    @Before
    fun init() {
        hiltRule.inject()
        IdlingRegistry.getInstance().register(EspressoIdlingResource.espressoTestIdlingResource)
        apiService.getDiscoverMovie(mapListQueries).blockingSingle().body()?.movieListItems?.apply {
            listMovie = this
        }
        apiService.getDiscoverTv(mapListQueries).blockingSingle().body()?.tvListItem?.apply {
            listTv = this
        }
        Thread.sleep(3000L)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.espressoTestIdlingResource)
    }


    @Test
    fun loadMovie() {
        onView(withId(R.id.rv_movie)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_movie)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                listMovie.size
            )
        )
    }

    @Test
    fun loadTv() {
        onView(withId(R.id.action_tv)).perform(click())
        onView(withId(R.id.rv_tv)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_tv)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                listTv.size
            )
        )
    }

    @Test
    fun loadDetailMovie() {
        onView(withId(R.id.rv_movie)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.movie_name)).check(matches(isDisplayed()))
        onView(withId(R.id.movie_name)).check(matches(withText(listMovie[0].title)))
    }

    @Test
    fun loadDetailTV() {
        onView(withId(R.id.action_tv)).perform(click())
        onView(withId(R.id.rv_tv)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.movie_name)).check(matches(isDisplayed()))
        onView(withId(R.id.movie_name)).check(matches(withText(listTv[0].original_name)))
    }

    @Test
    fun openTrailer() {
        onView(withId(R.id.rv_movie)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        onView(withId(R.id.movie_name)).check(matches(isDisplayed()))
        onView(withId(R.id.play_trailer)).perform(click())
        onView(withId(R.id.youtubePlayer)).check(matches(isDisplayed()))
    }


    @Test
    fun addLoadListAndRemoveFavoriteMovie() {
        onView(withId(R.id.rv_movie)).check(matches(isDisplayed()))
        for (i in 0 until totalFavoriteMovie) {
            onView(withId(R.id.rv_movie)).perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    i,
                    click()
                )
            )
            onView(withId(R.id.movie_name)).check(matches(isDisplayed()))
            onView(withId(R.id.favorite)).perform(click())
            pressBack()
        }
        onView(withId(R.id.favorite)).perform(click())
        onView(withId(R.id.rv_favorite_movie)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_favorite_movie)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                totalFavoriteMovie
            )
        )
        for (i in 0 until totalFavoriteMovie) {
            onView(withId(R.id.rv_favorite_movie)).perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    click()
                )
            )
            onView(withId(R.id.movie_name)).check(matches(isDisplayed()))
            onView(withId(R.id.favorite)).perform(click())
            pressBack()
        }
    }

    @Test
    fun addLoadListAndRemoveFavoriteTv() {
        onView(withId(R.id.action_tv)).perform(click())
        onView(withId(R.id.rv_tv)).check(matches(isDisplayed()))
        for (i in 0 until totalFavoriteTv) {
            onView(withId(R.id.rv_tv)).perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    i,
                    click()
                )
            )
            onView(withId(R.id.movie_name)).check(matches(isDisplayed()))
            onView(withId(R.id.favorite)).perform(click())
            pressBack()
        }
        onView(withId(R.id.favorite)).perform(click())
        onView(withId(R.id.tablayout)).perform(selectTabAtPosition(1))
        Thread.sleep(3000L)
        onView(withId(R.id.rv_favorite_tv)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_favorite_tv)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                totalFavoriteTv
            )
        )
        for (i in 0 until totalFavoriteTv) {
            onView(withId(R.id.rv_favorite_tv)).perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    0,
                    click()
                )
            )
            onView(withId(R.id.movie_name)).check(matches(isDisplayed()))
            onView(withId(R.id.favorite)).perform(click())
            pressBack()
        }
    }

    fun selectTabAtPosition(tabIndex: Int): ViewAction {
        return object : ViewAction {
            override fun getDescription() = "with tab at index $tabIndex"

            override fun getConstraints() =
                allOf(isDisplayed(), isAssignableFrom(TabLayout::class.java))

            override fun perform(uiController: UiController, view: View) {
                val tabLayout = view as TabLayout
                val tabAtIndex: TabLayout.Tab = tabLayout.getTabAt(tabIndex)
                    ?: throw PerformException.Builder()
                        .withCause(Throwable("No tab at index $tabIndex"))
                        .build()

                tabAtIndex.select()
            }
        }
    }
}